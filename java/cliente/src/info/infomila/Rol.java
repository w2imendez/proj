/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila;

/**
 *
 * @author Usuari
 */
public class Rol {
    private Integer codi;
    private String nom;

    public Integer getCodi() {
        return codi;
    }

    public void setCodi(Integer codi) {
        this.codi = codi;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Rol(Integer codi, String nom) {
        this.codi = codi;
        this.nom = nom;
    }
    
    
    
    
}
