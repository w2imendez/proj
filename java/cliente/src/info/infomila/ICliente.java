/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila;

import java.util.List;

/**
 *
 * @author Usuari
 */
public interface ICliente {
    boolean Login(String Login,String pass);
    void updateAdmin(String nuevopass);
    List<Usuario> getUsuarios();
    int addRol(int usuario,int rol);
    int checkAdmin();
    int checkTutor(int id);
    int checkFamilia(int id);
    int insertUsuario(String nom, String cognom, String cognom2, String login, String mail, String pass, List<Integer> roles );
    int UpdateaUsuario(int id,String nom, String cognom, String cognom2, String login, String mail, String pass, List<Integer> roles, Boolean passnum );
    int EliminaUsuario(int id);
}
