/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author Usuari
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Tenemos en args: " + args.length);
         if (args.length != 1 && args.length != 2) {
            System.out.println("L'execució d'aquest programa necessita d'1 o 2 arguments: ");
            System.out.println("Argument 1: Nom de la classe del component (obligatori)");
            System.out.println("Argument 2: Nom del fitxer de configuració que espera el component (optatiu)");
            System.exit(1);
        }
        String classeComponent = args[0];
        String nomFitxerConfiguracio = null;
        if (args.length == 2) {
            nomFitxerConfiguracio = args[1];
        }

        ICliente obj = null;
        Class c = null;
        try {
            c = Class.forName(classeComponent);
        } catch (ClassNotFoundException ex) {
            System.out.println("No es troba la classe " + classeComponent);
            System.exit(1);
        }
        try {
            if (nomFitxerConfiguracio == null) {
                Constructor x = c.getConstructor();
                obj = (ICliente) x.newInstance();
            } else {
                Constructor x = c.getConstructor(String.class);
                obj = (ICliente) x.newInstance(nomFitxerConfiguracio);
            }
             login aplication = new login(obj);
           int x =1;
            System.out.println("x =" + x);
           //JFrame aplication = new JFrame();
           //aplication.setSize(800,600);
             aplication.setVisible(true);
             aplication.setTitle("Usuario y Roles");
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            System.out.println("Problemes en intentar executar constructor de la " + classeComponent + " amb paràmetre=" + nomFitxerConfiguracio);
            System.out.println("Més informació:");
            Throwable cause = ex;
            do {
                if (cause.getMessage()!=null) System.out.println("\t"+cause.getMessage());
                cause = cause.getCause();                
            } while (cause!=null);
            System.exit(1);
        }
        
        /*
        try {
            /* Execució */
           
            System.out.println("Execució  ");
            
           
          /*
            List <Client> algo =obj.getClients();
            for(Client cc : algo){
                System.out.println(cc.getNumeroClient());
            }
            
          
            System.out.println("ya he lanzado");
           // obj.execute(guio);
        } catch (Exception ex) {
            System.out.println("Error: "+ex);
            Throwable cause = ex;
            do {
                if (cause.getMessage()!=null) System.out.println("\t"+cause.getMessage());
                cause = cause.getCause();                
            } while (cause!=null);
        }
        */
        
    }
    
}
