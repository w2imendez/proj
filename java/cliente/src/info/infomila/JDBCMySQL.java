/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuari
 */
public class JDBCMySQL implements ICliente{
     private Connection con;

    @Override
    public int addRol(int usuario, int rol) {
        return 0;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     
     public JDBCMySQL(String nomFitxerPropietats) throws Exception {
        if (nomFitxerPropietats == null) {
            nomFitxerPropietats = "jdbcMysql.txt";
        }
        Properties props = new Properties();
        try {
            props.load(new FileReader(nomFitxerPropietats));
        } catch (FileNotFoundException ex) {
            throw new Exception("No es troba fitxer de propietats", ex);
        } catch (IOException ex) {
            throw new Exception("Error en carregar fitxer de propietats", ex);
        }
        String driver = props.getProperty("driver");
        if (driver == null && System.getProperty("java.version").compareTo("1.6") < 0) {
            throw new Exception("La versió de Java obliga a definir la propietat driver dins el fitxer de propietats");
        }
        String url = props.getProperty("url");
        if (url == null) {
            throw new Exception("Manca la propietat url en el fitxer de propietats");
        }
        String user = props.getProperty("user");
        String password = props.getProperty("password");
        // No controlem !=null per què hi ha SGBDR que permeten no indicar user/contrasenya (MsAccess)
        try {
            if (driver != null) {
                Class.forName(driver);
            }
            con = DriverManager.getConnection(url, user, password);
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
           // con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
        } catch (SQLException ex) {
            throw new Exception("No es pot establir connexió", ex);
        } catch (ClassNotFoundException ex) {
            throw new Exception("No es pot carregar la classe " + driver);
        }
        // Connexió establerta
    }

    public JDBCMySQL() throws Exception {
        this("jdbcMysql.txt");
    }

    @Override
    public int checkAdmin() {
        int resultado = -1;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean x = false;
          String v = "select u.* from USUARIO u join UR urol on u.id = urol.usuario where urol.rol = 1";
         try {
             st = con.prepareStatement(v);
         } catch (SQLException ex) {
             
             Logger.getLogger(JDBCMySQL.class.getName()).log(Level.SEVERE, null, ex);
             return -1;
         }

         
           
         try {
              rs = st.executeQuery();
             if(rs.next()) {
                 if(rs.next()){
                     return 1;
                 }
                 else {
                     return -2;
                 }
             }
             else {
                 return -3;
             }
         } catch (SQLException ex) {
             Logger.getLogger(JDBCMySQL.class.getName()).log(Level.SEVERE, null, ex);
             return -1;
         }

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int checkTutor(int id) {
        try  {
            PreparedStatement st = null;
            ResultSet rs = null;
            boolean x = false;
            String v = "select a.* from ALUMNE a where a.tutor = ?";
            st= con.prepareStatement(v);
            st.setInt(1, id);
            rs = st.executeQuery();
            if(rs.next()){
                return -2;
            }
            else {
                return 1;
                /*
                rs.close();
                st.close();
                PreparedStatement st2 = null;
                ResultSet rs2 = null;
                String vv = "delete from USUARIO u where u.id = ? ";
                st2 = con.prepareStatement(vv);
                */
            }
            
        }
        catch(Exception e){
            return -1;
        }
    

      
// throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int checkFamilia(int id) {
        try  {
            PreparedStatement st = null;
            ResultSet rs = null;
            boolean x = false;
            String v = "select a.* from ALUMNE a where a.familia = ?";
            st= con.prepareStatement(v);
            st.setInt(1, id);
            rs = st.executeQuery();
            if(rs.next()){
                return -2;
            }
            else {
                return 1;
        
            }
            
        }
        catch(Exception e){
            return -1;
        }
        
     }

    @Override
    public boolean Login(String Login, String pass) {
         System.out.println("Intento de conexiion con logn" + Login + " y pass " + pass);
       
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean x = false;
        try {
            String v = "select p.nom as nom,usurol.rol as rol  from USUARIO p join UR usurol on (p.id = usurol.usuario) where p.login = ? and p.password = MD5(?) order by rol DESC";
            st = con.prepareStatement(v);

            st.setString(1, Login);
            st.setString(2, pass);
            rs = st.executeQuery();

            if (!rs.next()) {
                x = false;
                System.out.println("Salimos por que no hay un usuario con este login y pass");
            } else {
                 System.out.println(" Hay un usuario con este login y pass");
                int mirol = rs.getInt("rol");
                
                if(mirol == 1){
                    x = true;
                     System.out.println("Lo encontramos a la primera!");
                }
                else {
                    System.out.println("No Lo encontramos a la primera!");
                    while(rs.next()){
                       mirol = rs.getInt("rol");
                        System.out.println("en esta vuelta tenemos: " + mirol);
                       if(mirol == 1){
                            x = true;
                        }
                    }
                }
                /*
                while(rs.get)
                rs.get
                x = true;
*/
            }

            st.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return x;
        
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateAdmin(String nuevopass) {
         Statement st = null;
        ResultSet rs = null;
        ResultSet rs2 = null;

        String consulta = " UPDATE USUARIO  SET password = MD5(?) where login = ? ";

        try {
            PreparedStatement pstmt = con.prepareStatement(consulta);
            //pstmt.setInt(1, sinId);
            pstmt.setString(1, nuevopass);
            pstmt.setString(2, "admin");
            int x = pstmt.executeUpdate();
            con.commit();
            System.out.println("intento hacer la query de update credenciales admin con resultado.." + x );
//            return x;

        } catch (SQLException ex) {
            Logger.getLogger(JDBCMySQL.class.getName()).log(Level.SEVERE, null, ex);
  //          return 0;
        }
        

//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Usuario> getUsuarios() {
     List<Usuario> lista = new ArrayList();   
     PreparedStatement st = null;
        ResultSet rs = null;
        boolean x = false;
        
        String v = "select * from USUARIO";
         try {
             st = con.prepareStatement(v);
             rs = st.executeQuery();
             while(rs.next()){
             Usuario u = new Usuario(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4), rs.getString(5), rs.getString(6),rs.getString(7));
             lista.add(u);
             }
             st.close();
             rs.close();
         } catch (SQLException ex) {
             Logger.getLogger(JDBCMySQL.class.getName()).log(Level.SEVERE, null, ex);
         }
         for (Usuario u : lista) {
            PreparedStatement st2 = null;
            ResultSet rs2 = null;
             String vv = "select * from UR where usuario = ?";
         try {
             st2 = con.prepareStatement(vv);
             st2.setInt(1, u.getId());
             rs2 = st2.executeQuery();
             while(rs2.next()){
                 u.addRol(rs2.getInt(2));
             }
             st2.close();
             rs2.close();
             
         } catch (SQLException ex) {
             Logger.getLogger(JDBCMySQL.class.getName()).log(Level.SEVERE, null, ex);
         }
              
         }

        
     
     return lista;
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insertUsuario(String nom, String cognom, String cognom2, String login, String mail, String pass, List<Integer> roles) {
        try{
            
            PreparedStatement st4 = null;
            ResultSet rs4 = null;
            String v4 = "select * from USUARIO where login = ?";
            st4 = con.prepareStatement(v4);
            st4.setString(1, login);
            rs4 = st4.executeQuery();
            if(rs4.next()) return -2;
            st4.close();
            rs4.close();
            PreparedStatement st = null;
            ResultSet rs = null;
            String v = "select max(id) from USUARIO";
            st = con.prepareStatement(v);
            rs = st.executeQuery();
            rs.next();
            int max = rs.getInt(1);
            max++;
            PreparedStatement st2 = null;
            ResultSet rs2 = null;
            String v2 = "INSERT INTO USUARIO VALUES (?,?,?,?,?,MD5(?),?)";
            st2 = con.prepareStatement(v2);
            st2.setInt(1, max);
            st2.setString(2, nom);
            st2.setString(3, cognom);
            st2.setString(4, cognom2);
            st2.setString(5, login);
            st2.setString(6, pass);
            st2.setString(7, mail);
            st2.executeUpdate();
            st2.close();
            for (Integer i: roles){
                PreparedStatement st3 = null;
                String v3 = "INSERT INTO UR VALUES (?,?)";
                st3 = con.prepareStatement(v3);
                st3.setInt(1, max);
                st3.setInt(2, i);
                st3.executeUpdate();
                st3.close();
                con.commit();
            }
            
            
        }
        catch (SQLException ex){
             Logger.getLogger(JDBCMySQL.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
        
        
        return 1;

    
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int UpdateaUsuario(int id, String nom, String cognom, String cognom2, String login, String mail, String pass, List<Integer> roles, Boolean passnum) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    try{
        PreparedStatement st = null;
            ResultSet rs = null;
           
            PreparedStatement st2 = null;
            ResultSet rs2 = null;
            String v2 = "INSERT INTO USUARIO VALUES (?,?,?,?,?,MD5(?),?)";
            if(passnum){
                String v22= "UPDATE USUARIO SET nom = ?,cognom = ?, cognom2 = ?,login = ?,mail = ?,password = MD5(?) where id = ?";
                st2 = con.prepareStatement(v22);
                
                st2.setString(1, nom);
                st2.setString(2, cognom);
                st2.setString(3, cognom2);
                st2.setString(4, login);
                st2.setString(5, mail);
                st2.setString(6, pass);
                st2.setInt(7, id);
            }
            else {
                String v22= "UPDATE USUARIO SET nom = ?,cognom = ?, cognom2 = ?,login = ?,mail = ? where id = ?";
                st2 = con.prepareStatement(v22);
                st2.setString(1, nom);
                st2.setString(2, cognom);
                st2.setString(3, cognom2);
                st2.setString(4, login);
                st2.setString(5, mail);
                st2.setInt(6, id);                
            }
            st2.executeUpdate();
            st2.close();
            //Comprobacion de si hemos de borrar
            for(int i=1 ;i <4 ; i++ ){
                PreparedStatement st3 = null;
                ResultSet rs3 = null;
                String v4 = "select *  from UR where usuario = ? and rol = ?";
                st3 = con.prepareStatement(v4);
                st3.setInt(1, id);
                st3.setInt(2, i);
                rs3 = st3.executeQuery();
                if(rs3.next()){
                    if(!roles.contains(rs3.getInt(2))){
                        //hay que borrar
                        PreparedStatement st4 = null;
                        ResultSet rs4 = null;
                        String v5 = "delete from UR where usuario = ? and rol = ?";
                        st4 = con.prepareStatement(v5);
                        st4.setInt(1, id);
                        st4.setInt(2, i);
                        st4.executeUpdate();
                    }
                   
                    
                }
                
            }
            
            for (Integer i: roles){
                PreparedStatement st3 = null;
                ResultSet rs3 = null;
                String q = "select * from UR where usuario = ? and rol = ?";
                st3 = con.prepareStatement(q);
                st3.setInt(1, id);
                st3.setInt(2, i);
                rs3 = st3.executeQuery();
                if(!rs3.next()){
                     PreparedStatement st5 = null;
                     String q2 = "insert into UR values (?,?)";
                     st5 = con.prepareStatement(q2);
                     st5.setInt(1, id);
                     st5.setInt(2, i);
                     st5.executeUpdate();
                }
            }
            con.commit();
            
            
        }
        catch (SQLException ex){
             Logger.getLogger(JDBCMySQL.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
        
        
        return 1;
    

    }

    @Override
    public int EliminaUsuario(int id) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try {
            PreparedStatement st4 = null;
            ResultSet rs4 = null;
            String v5 = "delete from UR where usuario = ?";
            st4 = con.prepareStatement(v5);
            st4.setInt(1, id);
            st4.executeUpdate();
            
            PreparedStatement st5 = null;
            ResultSet rs5 = null;
            String v6 = "delete from USUARIO where id = ?";
            st5 = con.prepareStatement(v6);
            st5.setInt(1, id);
            st5.executeUpdate();
            con.commit();
            return 1;
            
        }
        catch(SQLException ex){
            return -1;
        }
        
        
     
    }
    
    
  
    
    
    
}
