/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.infomila;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuari
 */
public class Usuario {
    private int id;
    private String nom;
    private String cognom;
    private String cognom2;
    private String login;
    private String password;
    private String mail;
    private List<Integer> roles = new ArrayList();

    public Usuario(int id, String nom, String cognom, String cognom2, String login, String password, String mail) {
        this.id = id;
        this.nom = nom;
        this.cognom = cognom;
        this.cognom2 = cognom2;
        this.login = login;
        this.password = password;
        this.mail = mail;
    }

    protected Usuario() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public String getCognom2() {
        return cognom2;
    }

    public void setCognom2(String cognom2) {
        this.cognom2 = cognom2;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public List<Integer> getRoles() {
        return roles;
    }

    public void setRoles(List<Integer> roles) {
        this.roles = roles;
    }
    public void addRol(Integer rol) {
        this.roles.add(rol);
    }
    public void deleteRol(Integer rol){
        this.roles.remove(rol);
    }

    @Override
    public String toString() {
        return this.getLogin();
    }
    
            
    
}
