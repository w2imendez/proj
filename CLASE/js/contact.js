var infowindow; 
      window.addEventListener('load',function ()
      {
      // creaci� del mapa, per� encara no es dibuixa
          var myOptions = {
            //center: latlon
            zoom: 14,//4,
            mapTypeControl: false,
          //  mapTypeControlOptions:{style: google.maps.MapTypeControlStyle.BUTTON},// {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            // botons esquerra superior
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP   // ROADMAP,SATELLITE, HYBRID, TERRAIN
            }	
          map = new google.maps.Map(document.getElementById("infoMapa"), myOptions);
// si pemet geolocalitzaci�, fer la petici�
          if(geo_position_js.init()){
            geo_position_js.getCurrentPosition(success_callback,geoKO,{enableHighAccuracy:true});
          }
          else{
            alert("La Geolocalitzaci� ara no est� disponible");
          }
      });
		function success_callback(p)
		{
      
 
// Dibuixar mapa 
     // configuraci� d'un marcador a mostrar en el mapa
     // creaci� de la posici� on hi haur� el marcador
          var lat = p.coords.latitude;
          var longi = p.coords.longitude;
          var pos=new google.maps.LatLng(lat,longi);
     // modificaci� d'alguns par�metres / opcions del mapa          
          map.setCenter(pos);
          map.setZoom(18);

     // afegir un pop-up amb la informaci� que ens interessi
          infowindow = new google.maps.InfoWindow({
              content: "<strong>Ara amb marcador del punt exacte!</strong>"
          });
          
        //  infowindow = new google.maps.InfoWindow({});
        
      // creaci� de la marca
          var marker = new google.maps.Marker({
              position: pos,
              map: map,
              title:"Ets aqu�"
          });
/*
          google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
          });
      */    
         // Aqu� sota: m�tode per visualitzar el mapa, per� que ocupa tota la finestra, millor generar nova finestra 
      //geo_position_js.showMap(p.coords.latitude,p.coords.longitude);
	
      infowindow.open(map);//,marker);
      setTimeout(tancar,2000)
		}
    function tancar()
    {
        infowindow.close();
    }
function geoKO(error) {
       var sms = "Codi d'error: "+error.code+", AV�S: ";
    switch(error.code) {
        case error.PERMISSION_DENIED:
            sms = "L'usuari ha denegat la sol�licitud per Geolocalitzaci�"
            break;
        case error.POSITION_UNAVAILABLE:
            sms = "Informaci� sobre Localitzaci� inaccessible"
            break;
        case error.TIMEOUT:
            sms = "La sol�licitud per aconseguir la ubicaci� de l'usuari ha caducat"
            break;
        case error.UNKNOWN_ERROR:
            sms = "S'ha produ�t un error desconegut"
            break;
    }
    var t = document.createTextNode(sms);
}
