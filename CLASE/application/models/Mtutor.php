<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Mtutor. Summary. Descripció del fitxer
*/

/**
* Mtutor. operacion del modelo Mtutor
*
* Implementació de la classe Mtutor
* Part del model
*
* @author IM <w2.imendez@infomila.info>
*
* @package Mtutor
*/
class Mtutor extends CI_Model {
  /**
	 * Getter. Retorna Evaluaciones de un tutor
	 * @param string. tutor
	 * @return array|null Retorna array de evaluacion
	 */
  public function getEvaluaciones($tutor){
    $q = "select al.*,(select count(eva.alumne) from EVALUACION eva where alumne= al.id and trimestre = 1 ) as t1,(select count(eva.alumne) from EVALUACION eva where alumne= al.id and trimestre = 2 ) as t2,(select count(eva.alumne) from EVALUACION eva where alumne= al.id and trimestre = 3 ) as t3 from ALUMNE al where tutor = $tutor";
    $query = $this->db->query($q);
    return $query->result_array();
    
  }
  /**
	 * Getter. Retorna todas las evaluacion
	 * @return array|null Retorna array de evaluacion
	 */
  public function getAllEvaluaciones(){
        $q = "select * from EVALUACION";
        $query = $this->db->query($q);
        return $query->result_array();
  }
  /**
	 * Getter. Retorna todas las evaluaciones de un trimestrs
	 * @param string. tri
   * @return array|null Retorna array de evaluacion 
	 */
  public function getAllEvaluacionesTrimestre($tri){
   $q = "select * from EVALUACION where trimestre = $tri";
        $query = $this->db->query($q);
        return $query->result_array();
  }
  
    /**
	 * Getter. Retorna todas las evaluaciones de un alumno
	 * @param string. alum
   * @return array|null Retorna array de evaluacion 
	 */  
  public function getAllEvaluacionesAlumne($alum){
        $q = "select * from EVALUACION where alumne = $alum";
        $query = $this->db->query($q);
        return $query->result_array();
  }
    /**
	 * Getter. Retorna todas las de un alumno en un trimestre
	 * @param string. tri
   * @return array|null Retorna array de evaluacion 
	 */
  public function getEvaluacionesAlumne($alum,$tri){
     $q = "select * from EVALUACION where alumne = $alum and trimestre = $tri";
        $query = $this->db->query($q);
        return $query->result_array();
  }
  
    /**
	 * Getter. Retorna todas las evaluaciones de un tutor y nombre
	 * @param string. tutor
	 * @param string. nombre
   * @return array|null Retorna array de evaluacion 
	 */
    public function getEvaluacionesPorNombre($tutor,$nombre){
    $q = "select al.*,(select count(eva.alumne) from EVALUACION eva where alumne= al.id and trimestre = 1 ) as t1,(select count(eva.alumne) from EVALUACION eva where alumne= al.id and trimestre = 2 ) as t2,(select count(eva.alumne) from EVALUACION eva where alumne= al.id and trimestre = 3 ) as t3 from ALUMNE al where tutor = $tutor and al.nom like '%$nombre%'";
    $query = $this->db->query($q);
    return $query->result_array();
    
  }
    /**
	 * Getter. Retorna todas los items
   * @return array|null Retorna array de items 
	 */
   public function getItems(){
    $q = "select e.id as id, e.nom as nom, c.nom as categoria from ITEM e join CATEGORIA c on e.categoria = c.id order by c.nom";
    $query = $this->db->query($q);
    return $query->result_array();
    
  }
    /**
	 * Getter. Retorna todas las evaluaciones de un trimestrs
	 * @param string. tri
   * @return array|null Retorna array de evaluacion 
	 */
   
  public function getItemsAlumne($alum,$tri){
    $q = "select e.id as id, e.nom as nom, c.nom as categoria,ev.valoracion as valoracion,ev.trimestre as trimestre from ITEM e join CATEGORIA c on e.categoria = c.id  join EVALUACION ev on ev.item = e.id where ev.alumne = $alum and ev.trimestre = $tri order by c.nom";
    $query = $this->db->query($q);
    return $query->result_array();
    
  }
  public function insertEvaluacion($arr,$al,$tri,$usu){
    foreach ($arr as $clave => $valor){
      if($clave != "alumne" && $clave != "trimestre"){  
        $pieces = explode("-", $valor);
        $item = $pieces[0];
        $val = $pieces[1];
        $q = "insert into EVALUACION values ($usu,$al,$item,$tri,$val)";
          $query = $this->db->query($q);
      }
    }
  }
  public function getEvaluacion($al,$tri){
    
    $q = "select ev.valoracion as valoracion,al.nom as alum, ITEM.nom as itemnom from EVALUACION ev join ALUMNE al on (al.id = ev.alumne) join ITEM on ITEM.id = ev.item where ev.alumne = $al and ev.trimestre = $tri order by valoracion desc";
    
    
    /*
    $q = "select ev.valoracion as valoracion,al.nom as alum,it.nom as itemnom from evaluacion ev join alumne al on (al.id = ev.alumne) join item it on (ev.item = it.nom) where ev.alumne = $al and ev.trimestre = $tri order by valoracion desc";
    */
    // select ev.valoracion as valoracion,al.nom as alum, item.nom as itemnom from evaluacion ev join alumne al on (al.id = ev.alumne) join item on item.id = ev.item where ev.alumne = 3 and ev.trimestre = 2 order by valoracion desc
    $query = $this->db->query($q);
   
    return $query->result_array();
  }
    public function EliminaEvaluacion($al,$tri){
    $q= "delete from EVALUACION where alumne = $al and trimestre = $tri";
    $query = $this->db->query($q);
  }
  public function getAlumne($id){
  $q= "select * from ALUMNE where id = $id";
  $query = $this->db->query($q);
  return $query->result_array();
  }
  public function getAlumNom($prof,$nom){
    $q= "select nom from ALUMNE where tutor = $prof and nom like '%$nom%'";
     $query = $this->db->query($q);
  return $query->result_array();
  }

}