<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Madmin extends CI_Model {
  
  public function CheckAdmin($user,$pass){
    
    $pmd5 =md5($pass);
    $q = "select u.* from USUARIO u join UR urol where u.id = urol.usuario and urol.rol = 1 and login='$user' and password ='$pmd5'";
    $query = $this->db->query($q);

    return $query->result_array();

  }
  public function getUsuarios(){
    $q = "select u.nom as nom, u.cognom as cognom, u.cognom2 as cognom2 , u.login as login, u.mail as mail,r.nom as rol from USUARIO u join UR urol on u.id = urol.usuario join ROL r on r.codi = urol.rol";
    $query = $this->db->query($q);
    return $query->result_array();
  }
  public function getItems(){
    $q = "select * from ITEM";
    $query = $this->db->query($q);
    return $query->result_array();
  }
  public function getCategorias(){
    $q = "select * from CATEGORIA";
    $query = $this->db->query($q);
    return $query->result_array();
  }
  public function getAlumnos(){
    $q = "SELECT al.nom as nom ,al.cognom as cognom, al.cognom2 as cognom2, tutor.nom as tutor , familia.nom as  familia from ALUMNE al join USUARIO tutor on (al.tutor = tutor.id) join USUARIO familia on (al.familia = familia.id); ";
    $query = $this->db->query($q);
    return $query->result_array();
  }
  public function cargaUsuarios($csv){
    $q = "select max(id) as m from USUARIO";
    $query = $this->db->query($q);
    $res = $query->result_array();
    $cont = $res[0]['m'];
    $contal = 0;
    if($cont == null) $cont = 0;
    try {
  
    $data = array();
    foreach($csv as $key => $usuario){
      $cont++;
      $contal++;
      if(count($usuario) != 7){
        $e = array();
        array_push($e,-1);
        return $e;
      }
      $nom = $usuario[0];
      $cognom = $usuario[1];
      $cognom2 = $usuario[2];
      $login = $usuario[3];
      $password = $usuario[4];
      $mail = $usuario[5];
      $rol = $usuario[6];
      
      $q = "select * from USUARIO where login = '$login'";
      $query = $this->db->query($q);
      $v= $query->result_array();
      if(count($v)==0){
      
        $q = "insert into USUARIO values($cont,'$nom','$cognom','$cognom2','$login','$password','$mail')";
        $this->db->query($q);
        $q = "insert into UR values ($cont,$rol)";
        $this->db->query($q);
        $us = array();
        array_push($us,$nom);
        array_push($data,$us);
      }
    }
    return $data;
    } catch(Exception $e) {
      return -1;
      
    }
    
    
  }
  
  public function cargaItems($csv){
    $q = "select max(id) as m from ITEM";
    $query = $this->db->query($q);
    $res = $query->result_array();
    $cont = $res[0]['m'];
    $contal = 0;
    if($cont == null) $cont = 0;
    try {
  
    $data = array();
    foreach($csv as $key => $usuario){
      $cont++;
      if(count($usuario) != 2){
        $e = array();
        array_push($e,-1);
        return $e;
      }
      $nom = $usuario[0];
      $categoria = $usuario[1];
        $q = "select * from ITEM where nom = '$nom'";
      $query = $this->db->query($q);
      $v= $query->result_array();
      if(count($v) == 0) {
      $q = "insert into ITEM values($cont,'$nom','$categoria')";
      $this->db->query($q);
 
      $us = array();
      array_push($us,$nom);
      array_push($data,$us);
      }
    }
    return $data;
    } catch(Exception $e) {
      return -1;
      
    }
    
    
  }
  public function cargaCategorias($csv){
    $q = "select max(id) as m from CATEGORIA";
    $query = $this->db->query($q);
    $res = $query->result_array();
    $cont = $res[0]['m'];
    $contal = 0;
    if($cont == null) $cont = 0;
    try {
  
    $data = array();
    foreach($csv as $key => $usuario){
      $cont++;
      if(count($usuario) != 1){
        $e = array();
        array_push($e,-1);
        return $e;
      }
      $nom = $usuario[0];

    $q = "select * from CATEGORIA where nom = '$nom'";
      $query = $this->db->query($q);
      $v= $query->result_array();
      if(count($v) == 0) {
      $q = "insert into CATEGORIA values($cont,'$nom')";
      $this->db->query($q);
 
      $us = array();
      array_push($us,$nom);
      array_push($data,$us);
      }
    }
    return $data;
    } catch(Exception $e) {
      return -1;
      
    }
    
    
  }
  public function cargaAlumnos($csv){
    $q = "select max(id) as m from ALUMNE";
    $query = $this->db->query($q);
    $res = $query->result_array();
    $cont = $res[0]['m'];
    $contal = 0;
    if($cont == null) $cont = 0;
    try {
  
    $data = array();
    foreach($csv as $key => $usuario){
      $cont++;
      if(count($usuario) != 5){
        $e = array();
        array_push($e,-1);
        return $e;
      }
      $nom = $usuario[0];
      $cognom = $usuario[1];
      $cognom2 = $usuario[2];
      $familia =  $usuario[3];
      $tutor =  $usuario[4];
      $q = "select * from ALUMNE where nom = '$nom' and familia = $familia";
      $query = $this->db->query($q);
      $v= $query->result_array();
      if(count($v) == 0) {
      $q = "insert into ALUMNE values($cont,'$nom','$cognom','$cognom2',$familia,$tutor)";
      $this->db->query($q);
 
      $us = array();
      array_push($us,$nom);
      array_push($data,$us);
      }
    }
    return $data;
    } catch(Exception $e) {
      return -1;
      
    }
    
    
  }
  
  

}