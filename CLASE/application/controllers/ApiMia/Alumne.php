<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Alumne extends REST_Controller {
    var $limite = false;
    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['empleatsdpt_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['empleatsdpt_post']['limit'] = 100; // 100 requests per hour per user/key
    }

    
    public function evaluacion_get()
    {
     
      
      $alum = $this->get("alumne");
      $tri = $this->get("trimestre");
      $this->load->model('Mtutor');
      $ok = null;
      if($alum == null && $tri == null) {
        $ok = $this->Mtutor->getAllEvaluaciones();
      }
      else {
        if($alum == null){
          $ok = $this->Mtutor->getAllEvaluacionesTrimestre($tri);
        }
        else if ($tri == null){
          $ok = $this->Mtutor->getAllEvaluacionesAlumne($alum);
        }
        else {
          $ok = $this->Mtutor->getEvaluacionesAlumne($alum,$tri);
        }
      }

        if (!empty($ok))
        {
            $this->set_response($ok, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Info could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }      
       
      
    }
    public function empleatsdpt_get()
    {
        // Users from a data store e.g. database
        $emps = [
            ['id' => 1, 'name' => 'John', 'email' => 'john@example.com', 'dpt' => 'Informatica'],
            ['id' => 2, 'name' => 'Jim', 'email' => 'jim@example.com', 'dpt' => 'Pelacables'],
            ['id' => 3, 'name' => 'Jane', 'email' => 'jane@example.com', 'dpt' => 'Alma en pena de Marketing',],
        ];

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($emps)
            {
                // Set the response and exit
                $this->response($emps, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No emps were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $emp = NULL;

        if (!empty($emps))
        {
            foreach ($emps as $key => $value)
            {
                if (isset($value['id']) && $value['id'] === $id)
                {
                    $emp = $value;
                }
            }
        }
            
            
                
        

        if (!empty($emp) && !$limite )
        {
            $this->set_response($emp, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Emps could not be found '
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
        }
    }
     public function empleatsdptv2_get()
    {
        // Users from a data store e.g. database
        $emps = [
            ['id' => 5, 'name' => 'John', 'email' => 'john@example.com', 'dpt' => 'Informatica'],
            ['id' => 6, 'name' => 'Jim', 'email' => 'jim@example.com', 'dpt' => 'Pelacables'],
            ['id' => 7, 'name' => 'Jane', 'email' => 'jane@example.com', 'dpt' => 'Alma en pena de Marketing',],
        ];

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($emps)
            {
                // Set the response and exit
                $this->response($emps, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No emps were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $emp = NULL;

        if (!empty($emps))
        {
            foreach ($emps as $key => $value)
            {
                if (isset($value['id']) && $value['id'] === $id)
                {
                    $emp = $value;
                }
            }
        }

        if (!empty($emp))
        {
            $this->set_response($emp, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Emps could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
    public function user_put(){
        $f = fopen ('xxx', 'w');
        parse_str (file_get_contents('php://input'), $a);
        fputs ($a, $f);
         fclose ($f);
         exit;
          
      $nom = $this->post('nom');
      $pa = $this->post('password');
      $this->load->database();
      $this->load->model('User');
      $ok = $this->User->putUser($nom,$pa);
      $this->set_response($emp, REST_Controller::HTTP_OK);
      /*
      if($ok){
          $this->set_response($emp, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      }
      else {
         $this->set_response([
                'status' => FALSE,
                'message' => 'fallo al insertar'
            ], REST_Controller::SERVER_ERROR); // NOT_FOUND (404) being the HTTP response code
        
      }
      */
      //return $ok;
      
    }
    
     public function user_post(){
        $f = fopen ('xxx', 'w');
      //  parse_str (file_get_contents('php://input'), $a);
        $a = $this->post('nom');
        fputs ($a, $f);
         fclose ($f);
         exit;
          
      $nom = $this->post('nom');
      $pa = $this->post('password');
      $this->load->database();
      $this->load->model('User');
      $ok = $this->User->putUser($nom,$pa);
      $this->set_response($emp, REST_Controller::HTTP_OK);
      /*
      if($ok){
          $this->set_response($emp, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      }
      else {
         $this->set_response([
                'status' => FALSE,
                'message' => 'fallo al insertar'
            ], REST_Controller::SERVER_ERROR); // NOT_FOUND (404) being the HTTP response code
        
      }
      */
      //return $ok;
      
    }
    

    public function empleatsdpt_post()
    {
      $id = $this->post('id');
       $emps = [
            ['id' => 1, 'name' => 'John', 'email' => 'john@example.com', 'dpt' => 'Informatica'],
            ['id' => 2, 'name' => 'Jim', 'email' => 'jim@example.com', 'dpt' => 'Pelacables'],
            ['id' => 3, 'name' => 'Jane', 'email' => 'jane@example.com', 'dpt' => 'Alma en pena de Marketing',],
        ];


        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($emps)
            {
                // Set the response and exit
                $this->response($emps, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No emps were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $emp = NULL;

        if (!empty($emps))
        {
            foreach ($emps as $key => $value)
            {
                if (isset($value['id']) && $value['id'] === $id)
                {
                    $emp = $value;
                }
            }
        }

        if (!empty($emp))
        {
            $this->set_response($emp, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Emps could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
      
      /*
        // $this->some_model->update_user( ... );
        $message = [
            'id' => 100, // Automatically generated by the model
            'name' => $this->post('name'),
            'email' => $this->post('email'),
            'message' => 'Added a resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
        */
    }

 

}
