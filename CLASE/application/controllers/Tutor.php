<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tutor extends CI_Controller {
  public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	public function index()
	{
    if(!isset($_SESSION['tutor'])) {
       redirect('/Login/index', 'refresh');
    }
    $this->load->model('Mtutor');
    //var_dump($_SESSION['tutor']);
    
    $res = $this->Mtutor->getEvaluaciones($_SESSION['tutor']["id"]);
    $data['res'] = $res;
    $data['tut'] = $_SESSION['tutor']['nom'];
		$this->load->view('tutorgen',$data);
    $this->load->view('footer');
	
  }
  public function filtro(){
     if(!isset($_SESSION['tutor'])) {
       redirect('/Login/index', 'refresh');
    }
    $this->load->model('Mtutor');
    //var_dump($_SESSION['tutor']);
    
    $res = $this->Mtutor->getEvaluacionesPorNombre($_SESSION['tutor']["id"],$this->input->post('autocompleta'));
    $data['res'] = $res;
        $data['tut'] = $_SESSION['tutor']['nom'];
		$this->load->view('tutorgen',$data);
    $this->load->view('footer');
  }
  
  public function creaevaluacion($al,$tri){
    if(!isset($_SESSION['tutor'])) {
       redirect('/Login/index', 'refresh');
    }
    $data['al'] = $al;
    $data['tri'] = $tri;
	$data['tut'] = $_SESSION['tutor']['nom'];
    $this->load->model('Mtutor');
    $res = $this->Mtutor->getItems();
    $data['res'] = $res;
    $this->load->view('tutorcrea',$data);
    
    
  }
  public function guardaevaluacion(){
    //var_dump($_POST);
 if(!isset($_SESSION['tutor'])) {
       redirect('/Login/index', 'refresh');
    }
    $arr = $_POST;
    $this->load->model('Mtutor');
    $this->Mtutor->insertEvaluacion($arr,$_POST['alumne'],$_POST['trimestre'],$_SESSION['tutor']["id"]);
    $alumne = $this->Mtutor->getAlumne($_POST['alumne']);
    $res = $this->Mtutor->getEvaluacion($_POST['alumne'],$_POST['trimestre']);
    $this->load->library("Pdf");
    $data['res'] = $res;
    $data['alum'] = $alumne[0]["nom"];
    $data['tutor'] = $_SESSION['tutor']["nom"];
    $data['tri'] = $_POST['trimestre'];
    $this->load->view("pdf",$data);
   
  }
  public function eliminaevaluacion($alum,$tri) {
     if(!isset($_SESSION['tutor']) || $alum == null || $tri == null) {
       redirect('/Login/index', 'refresh');
    }
    $this->load->model('Mtutor');
    $this->Mtutor->EliminaEvaluacion($alum,$tri);
     redirect('/Tutor/index', 'refresh');
    /*
    $res = $this->Mtutor->getEvaluacion($alum,$tri);
    $data['res'] = $res;
    $data['alum'] = $alumne[0]["nom"];
    $data['tutor'] = $_SESSION['tutor']["nom"];
    $data['tri'] = $_POST['trimestre'];
    $this->load->view("modificapdf",$data);
    */
    
  }
    public function modificaevaluacion($alum,$tri) {
     if(!isset($_SESSION['tutor']) || $alum == null || $tri == null) {
       redirect('/Login/index', 'refresh');
    }
    $this->load->model('Mtutor');
    	$data['tut'] = $_SESSION['tutor']['nom'];
   $res = $this->Mtutor->getItemsAlumne($alum,$tri);
    $data['res'] = $res;
    $alumne = $this->Mtutor->getAlumne($alum);
    $data['al'] = $alum;
    $data['tri'] = $tri;
    $this->load->view("tutormodifica",$data);
    
    
  }
    public function modevaluacion(){
    //var_dump($_POST);
 if(!isset($_SESSION['tutor'])) {
       redirect('/Login/index', 'refresh');
    }
    $arr = $_POST;
    $this->load->model('Mtutor');
     $this->Mtutor->EliminaEvaluacion($_POST['alumne'],$_POST['trimestre']);
    $this->Mtutor->insertEvaluacion($arr,$_POST['alumne'],$_POST['trimestre'],$_SESSION['tutor']["id"]);
    /*
    $alumne = $this->Mtutor->getAlumne($_POST['alumne']);
    
    $res = $this->Mtutor->getEvaluacion($_POST['alumne'],$_POST['trimestre']);
    $this->load->library("Pdf");
    $data['res'] = $res;
    $data['alum'] = $alumne[0]["nom"];
    $data['tutor'] = $_SESSION['tutor']["nom"];
    $data['tri'] = $_POST['trimestre'];
    $this->load->view("pdf",$data);
   */
    redirect('/Tutor/index', 'refresh');
  }
  public function descargaevaluacion($al,$tri) {
    $this->load->model('Mtutor');
    $alumne = $this->Mtutor->getAlumne($al);
    $res = $this->Mtutor->getEvaluacion($al,$tri);
    $this->load->library("Pdf");
    $data['res'] = $res;
    $data['alum'] = $alumne[0]["nom"];
    $data['tutor'] = $_SESSION['tutor']["nom"];
    $data['tri'] = $tri;
    $this->load->view("pdf",$data);
  }
  public function logout(){
       $_SESSION['tutor']= null;
      redirect('/Login/index', 'refresh');
  }
  
  
}
