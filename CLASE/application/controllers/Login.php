<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
  public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('login');
    //$this->load->view('footer');
	}
  public function admin(){
    $this->load->model('Mlogin');
    $user = $this->input->post("usernameAdmin");
    $pass = $this->input->post("passwordadmin");
    
    $res = $this->Mlogin->CheckAdmin($user,$pass);
    if(count($res)== 0){
       redirect('/Login/index', 'refresh');
    }
    else {
      $_SESSION['admin'] = $res[0];
       redirect('/Admin/index', 'refresh');
    }
   
  }
  
  public function familia(){
    $this->load->model('Mlogin');
    $user = $this->input->post("usuariofamilia");
    $pass = $this->input->post("passwordalumno");
    
    $res = $this->Mlogin->CheckFamilia($user,$pass);
    if(count($res)== 0){
       redirect('/Login/index', 'refresh');
    }
    else {
      $_SESSION['familia'] = $res[0];
       redirect('/Familia/index', 'refresh');
    }
    
  }
  
  public function tutor() {
    $this->load->model('Mlogin');
    $user = $this->input->post("logintutor");
    $pass = $this->input->post("passwordtutor");
    
    $res = $this->Mlogin->CheckTutor($user,$pass);
    if(count($res)== 0){
       redirect('/Login/index', 'refresh');
    }
    else {
      $_SESSION['tutor'] = $res[0];
       redirect('/Tutor/index', 'refresh');
    }
    
  }

}
