<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Familia extends CI_Controller {
  public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	public function index()
	{
    if(!isset($_SESSION['familia'])) {
       redirect('/Login/index', 'refresh');
    }
    $this->load->model('Mfamilia');
    //var_dump($_SESSION['tutor']);
    
    $res = $this->Mfamilia->getEvaluaciones($_SESSION['familia']["id"]);
    $data['res'] = $res;
    $data['familia'] = $_SESSION['familia']['nom'];
		$this->load->view('familia',$data);
  
   
    $this->load->view('footer');
	
  }
 
  public function logout(){
       $_SESSION['familia']= null;
      redirect('/Login/index', 'refresh');
  }
  
  
}
