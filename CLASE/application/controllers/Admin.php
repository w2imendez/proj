<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
  public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	public function index()
	{
    if(!isset($_SESSION['admin'])) {
       redirect('/Login/index', 'refresh');
    }
		$this->load->view('admingen');
    $this->load->view('footer');
	}
  
  public function usuario(){
    if(!isset($_SESSION['admin'])) {
       redirect('/Login/index', 'refresh');
    }
    $this->load->model('Madmin');
    $usu = $this->Madmin->getUsuarios();   
    $data['usu']= $usu;
    $this->load->view('adminusuario',$data); 
     $this->load->view('footer');
  }
  
  public function logout() {
      $_SESSION['admin']= null;
      redirect('/Login/index', 'refresh');
  }
  public function items(){
    if(!isset($_SESSION['admin'])) {
       redirect('/Login/index', 'refresh');
    }
    $this->load->model('Madmin');
    $items = $this->Madmin->getItems();   
    $data['items']= $items;
    $this->load->view('adminitems',$data);
     $this->load->view('footer');
  }
  public function categoria(){
    if(!isset($_SESSION['admin'])) {
       redirect('/Login/index', 'refresh');
    }
    $this->load->model('Madmin');
    $categoria = $this->Madmin->getCategorias();   
    $data['categorias']= $categoria;
    $this->load->view('admincategorias',$data);
     $this->load->view('footer');
  }
  public function alumno(){
    if(!isset($_SESSION['admin'])) {
       redirect('/Login/index', 'refresh');
    }
    $this->load->model('Madmin');
    $alumnos = $this->Madmin->getAlumnos();   
    $data['alumnos']= $alumnos;
    $this->load->view('adminalumnos',$data);
     $this->load->view('footer');
  }
  public function subeusuario(){
    if(!isset($_SESSION['admin'])) {
       redirect('/Login/index', 'refresh');
    }
    if(isset($_FILES['usuarioupload'])){
      $file_name = $_FILES['usuarioupload']['name'];
      $file_size =$_FILES['usuarioupload']['size'];
      $file_tmp =$_FILES['usuarioupload']['tmp_name'];
      $file_type=$_FILES['usuarioupload']['type'];
      if($file_size == 0) redirect('/admin/index', 'refresh');
      $tmp= explode('.',$file_name);
      $file_ext=strtolower(end($tmp));
      $expensions= array("csv");
      //subida al server
      move_uploaded_file($file_tmp,"uploads/".$file_name);
      //leer el archivo
      $csv = array_map('str_getcsv', file("uploads/".$file_name));
      $this->load->model('Madmin');
      $res = $this->Madmin->cargaUsuarios($csv);
      array_push($res, 'usuario/s');
      $data['res'] = $res;
      $this->load->view('admingen',$data);
       $this->load->view('footer');

    }
    else {
       redirect('/Login/index', 'refresh');
    }
  }

  public function subeitem(){
    if(!isset($_SESSION['admin'])) {
       redirect('/Login/index', 'refresh');
    }
    if(isset($_FILES['itemsupload'])){
      $file_name = $_FILES['itemsupload']['name'];
      $file_size =$_FILES['itemsupload']['size'];
      $file_tmp =$_FILES['itemsupload']['tmp_name'];
      $file_type=$_FILES['itemsupload']['type'];
      if($file_size == 0) redirect('/admin/index', 'refresh');
      $tmp= explode('.',$file_name);
      $file_ext=strtolower(end($tmp));
      $expensions= array("csv");
      //subida al server
      move_uploaded_file($file_tmp,"uploads/".$file_name);
      //leer el archivo
      $csv = array_map('str_getcsv', file("uploads/".$file_name));
      $this->load->model('Madmin');
      $res = $this->Madmin->cargaItems($csv);
      array_push($res, 'item/s');
      $data['res'] = $res;
      $this->load->view('admingen',$data);
       $this->load->view('footer');

    }
    else {
       redirect('/Login/index', 'refresh');
    }
  }
  public function subecategoria(){
    if(!isset($_SESSION['admin'])) {
       redirect('/login/index', 'refresh');
    }
    if(isset($_FILES['categoriaupload'])){
      $file_name = $_FILES['categoriaupload']['name'];
      $file_size =$_FILES['categoriaupload']['size'];
      $file_tmp =$_FILES['categoriaupload']['tmp_name'];
      $file_type=$_FILES['categoriaupload']['type'];
      if($file_size == 0) redirect('/admin/index', 'refresh');
      $tmp= explode('.',$file_name);
      $file_ext=strtolower(end($tmp));
      $expensions= array("csv");
      //subida al server
      move_uploaded_file($file_tmp,"uploads/".$file_name);
      //leer el archivo
      $csv = array_map('str_getcsv', file("uploads/".$file_name));
      $this->load->model('Madmin');
      $res = $this->Madmin->cargaCategorias($csv);
      array_push($res, 'categoria/s');
      $data['res'] = $res;
      $this->load->view('admingen',$data);
       $this->load->view('footer');

    }
    else {
       redirect('/Login/index', 'refresh');
    }
  }
    public function subealumno(){
    if(!isset($_SESSION['admin'])) {
       redirect('/Login/index', 'refresh');
    }
    if(isset($_FILES['alumnoupload'])){
      $file_name = $_FILES['alumnoupload']['name'];
      $file_size =$_FILES['alumnoupload']['size'];
      $file_tmp =$_FILES['alumnoupload']['tmp_name'];
      $file_type=$_FILES['alumnoupload']['type'];
      if($file_size == 0) redirect('/admin/index', 'refresh');
      $tmp= explode('.',$file_name);
      $file_ext=strtolower(end($tmp));
      $expensions= array("csv");
      //subida al server
      move_uploaded_file($file_tmp,"uploads/".$file_name);
      //leer el archivo
      $csv = array_map('str_getcsv', file("uploads/".$file_name));
      $this->load->model('Madmin');
      $res = $this->Madmin->cargaAlumnos($csv);
      array_push($res, 'Alumno/s');
      $data['res'] = $res;
      $this->load->view('admingen',$data);
       $this->load->view('footer');

    }
    else {
       redirect('/Login/index', 'refresh');
    }
  }
}
