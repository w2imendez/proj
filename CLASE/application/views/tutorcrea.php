
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="Author" content="Ivan">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>font-awesome/css/fontawesome-all.css">
    <script src="<?php echo base_url();?>js/tutorcrea.js"></script>
		<title>
			MENUDA PRACTICA...
		</title>
    

	</head>
  <body>
  <header>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Panel de Control de <?php echo $tut ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo site_url('Tutor/index'); ?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">Mis Alumnos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">Mi clases</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">Mis horarios</a>
      </li>
      
    </ul>
    
    <span class="navbar-text">
       <a class="btn btn-danger" href="<?php echo site_url('Tutor/logout'); ?>" role="button">Logout</a>
    </span>
  </div>
</nav>
</header>
<main>
<form method="POST" action="<?php echo site_url('Tutor/guardaevaluacion'); ?>">
<table class="table">
  <thead>
    <tr>
      <th scope="col"><?php echo $al; echo  " - "; echo $tri; ?></th>
      <th scope="col">Molt Satisfactoriament</th>
      <th scope="col">Satisfactoriament</th>
      <th scope="col">Acceptable</th>
      <th scope="col">Cal que millori</th>
      <th scope="col">No evaluat</th>
    </tr>
  </thead>
  <tbody>
  <?php
  $categoria  = "";
  foreach($res as $data){
    if($data['categoria'] != $categoria) {
      $categoria = $data['categoria'];
     
    
        
      echo "</tbody>";
      echo "<thead><tr><th colspan='6'> $categoria </th></tr></thead>";
      echo "<tbody>";
      
      
    }

      
      echo "<tr><td>". $data['nom']. "</td> <td> <input type='radio' name='radio_list_".$data['id'] . "' value='" . $data['id'] . "-5'></td>".
      "<td> <input type='radio' name='radio_list_" . $data['id'] ."' value='" . $data['id'] . "-4'></td>".
   "<td> <input type='radio' name='radio_list_" . $data['id'] ."' value='" . $data['id'] . "-3'></td>".
    "<td> <input type='radio' name='radio_list_" . $data['id'] ."' value='" . $data['id'] . "-2'></td>".
     "<td> <input type='radio' name='radio_list_" . $data['id'] ."' value='" . $data['id'] . "-1'></td>".
      "</tr>";
    
  }
  echo "</tbody>";

  ?>
</table>
      <button disabled id="creabtn" class="btn btn-primary" type="submit" >Guarda</button>
    <input type="hidden" name="alumne" value="<?php echo $al; ?>">
    <input type="hidden" name="trimestre" value="<?php echo $tri; ?>">
</form>


        

</main>
	</body>

	</html>
