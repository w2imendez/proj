<?php

  $pdf = new TCPDF("P",'mm','A4',true,'UTF-8',false);    
 
    $pdf->SetCreator("eo");
    $pdf->SetAuthor('Ivan Mendez ');
    $pdf->SetSubject('notas');
    $pdf->SetDisplayMode('real','default');
    $pdf->SetPrintHeader(true);
$pdf->SetPrintFooter(true);
$date = date('m/d/Y h:i:s a', time());
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "Informe generado para $alum Trimestre: $tri", "Informe generado por  $tutor el dia de " . $date );

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    $pdf->AddPage(); 

$pre = array(array(),array("La capacidad de","Lamentablemente, se ha de decir que","no se ha podido calicar la habilidad de","Es un misterio en cuanto a"),array("La capacidad de","Es necesario decir que","Demuestra dificultad en cuanto a", "Es nefasto en cuanto a"),array("la capacidad de","Se ha de decir que","Es capaz de","Es pasable en cuanto a"),array("Es remarcable la capacidad de","Cabe destacar que","Demuestra soltura para","Es muy bueno en cuanto a"),array("Es muy remarcable la capacidad de", "Cabe destacar que","Demuestra gran soltura para","Es excelente en cuanto a"));

$pro = array(array(),array("no ha sido evaluada","no ha sido evaluado", "por falta de oportunidad","respecta"),array("necesita mejorse","necesita mejorar","","respecta"),array("esta aun nivel aceptable","no resulta un problema", ", aun que con peque�as dificultades","pertoca" ),array("a un nivel satisfactorio","parece ser un punto fuerte para el alumno","sin problema","pertoca"),array("a un nivel muy satisfactorio","parece ser un punto muy fuerte para el alumno","sin problema","pertoca"));

$evaluacion = -1;
$cont = 0;

      for($i = 0; $i < count($res); ++$i) {
   
    if($res[$i]['valoracion'] != $evaluacion) {
      $evaluacion = $res[$i]['valoracion'];
      $cont = 0;
      //$pdf->MultiCell(0,5,utf8_decode(chr(10)),1);
      //$pdf->MultiCell(0,5,utf8_decode(chr(10)),1);
      if($evaluacion == 5) {
            $pdf->Write(5,"\n");
          $pdf->Write(5,'El alumno demuestra excelencia en los siguientes apartados: ');
           $pdf->Write(5,"\n");
      }
      if($evaluacion == 4) {
            $pdf->Write(5,"\n");
          $pdf->Write(5,'El alumno demuestra proficiencia con los siguientes items: ');
           $pdf->Write(5,"\n");
      }
      if($evaluacion == 3) {
            $pdf->Write(5,"\n");
          $pdf->Write(5,'El alumno ha consolidado los siguientes apartados: ');
           $pdf->Write(5,"\n");
      }
      
      if($evaluacion == 2) {
            $pdf->Write(5,"\n");
          $pdf->Write(5,'El alumno debe de aplicarse mas respecto los siguientes items: ');
             $pdf->Write(5,"\n");
      }
      if($evaluacion == 1) {
            $pdf->Write(5,"\n");
          $pdf->Write(5,'El alumno no ha sido evaluado en ciertos aspectos: ');
          $pdf->Write(5,"\n");
        
      }
    }
      $valor = rand(0,3);
     //$valor = 1;
     /*
     $width = 100;
$lineHeight = 4;

$pdf->Multicell(0,2,"This is a multi-line text string\nNew line\nNew line");
*/
 $pdf->Write(5,"\n");
    //$pdf->MultiCell(0,5,utf8_decode(chr(10)),1);
     $pdf->Write(5,"   ".$pre[$evaluacion][$valor]." " . $res[$i]["itemnom"] . " " . $pro[$evaluacion][$valor]);
  }
//file_put_contents(base_url().'pdfs/caca'.".pdf", $pdf->output());
ob_clean();
    $pdf->Output(base_url()."Notas.pdf", 'I');
exit;  
  ?>