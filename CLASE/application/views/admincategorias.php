
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="Author" content="Ivan">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>font-awesome/css/fontawesome-all.css">
				<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/admin.css">

		<title>
			MENUDA PRACTICA...
		</title>
    

	</head>
  <body>
  <header>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Panel de Control</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link" href="<?php echo site_url('admin/index'); ?>">Home </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/usuario'); ?>">Usuario</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/items'); ?>">Items</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo site_url('admin/categoria'); ?>">Categorias <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/alumno'); ?>">Alumnos</a>
      </li>
    </ul>
  
    <span class="navbar-text">
       <a class="btn btn-danger" href="<?php echo site_url('admin/logout'); ?>" role="button">Logout</a>
    </span>
  </div>
</nav>
</header>
<main>
<div class="container cartas">
			<?php $i=0; ?>
			<?php if (isset($categorias)) { ?>
			<div class="card-deck">
				<?php foreach ($categorias as $data) { ?>
				<?php if(($i%2)==0) echo "</div><div class='card-deck'>"; ?>
				<div class="card border-faded">
					<div class="card-body">
						<div class="row">
							<div class="col-4 text-left">
								<h5 class="card-title  text-left ">
									<?php echo $data['id']; ?>
								</h5>
							</div>
							
							<div class="col-8 text-right">
								<h5 class="card-title  text-right">
									<?php echo $data['nom']; ?>
								</h5>

								</div>
						</div>			
					</div>
				</div>
				<?php $i++; ?>
				<?php } ?>
				<?php } else { ?>
				<div><div class="alert alert-danger text-center" style="margin:15px;" role="alert">No existen Categorias!</div></div>
			<?php } ?>
			</div>
		</div>


<div class="container" id="mitexto">
Desde este nuestro centro de la excelencia es muy importante para nosotros... 
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam elementum quis nisl in malesuada. Nam ac tincidunt ex. Donec condimentum tellus purus, non porta nibh imperdiet eget. Vestibulum fringilla cursus urna quis tincidunt. Morbi id mauris ac mauris scelerisque varius nec non lacus. Pellentesque luctus porta turpis non elementum. Nunc quam risus, semper in massa a, dictum semper justo. Cras mattis congue hendrerit. Suspendisse dapibus diam sit amet sapien fringilla, vitae iaculis ipsum sodales. Nunc vestibulum porta hendrerit. Suspendisse ornare ex non volutpat volutpat. Fusce eget pretium leo, in faucibus augue. Duis fermentum ligula lacus, ut pretium nisl viverra nec. Vestibulum suscipit, diam ut venenatis pulvinar, metus erat venenatis eros, id fermentum nunc tellus ac nisl. Quisque quis porttitor elit. Aliquam efficitur dui id scelerisque finibus.

Aliquam rutrum dolor id lorem consectetur, at tincidunt nibh lacinia. Sed ullamcorper cursus nulla, non pellentesque ex congue non. Suspendisse potenti. Aenean felis purus, ultricies at sapien eget, congue imperdiet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer id iaculis nulla. Pellentesque posuere nisl vel efficitur hendrerit. In eu venenatis purus. Donec posuere neque lacinia augue dignissim posuere. In sit amet congue augue, sit amet fermentum magna. Quisque dictum justo ut iaculis ultricies. Nunc eu erat mauris. Phasellus volutpat, metus quis dignissim sollicitudin, libero dolor sollicitudin lacus, non consequat ex elit rutrum lectus. Aliquam nisl odio, rutrum eu arcu nec, mattis mollis felis. Fusce fringilla nisi eu risus dignissim, sit amet pharetra odio placerat. Nulla tincidunt laoreet magna, ac pretium quam.
</div>
        

</main>
	</body>

	</html>
