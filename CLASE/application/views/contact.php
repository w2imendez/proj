<!DOCTYPE html> 
<html>
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="Author" content="Ester Marsal Roca">   
	<script src="<?php echo base_url(); ?>js/geo-min.js"></script>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>font-awesome/css/fontawesome-all.css">
<meta name = "viewport" content = "width = device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">		
    <link rel="stylesheet" href="<?php echo base_url();?>css/contact.css">
<!-- fent proves
<script src="js/geo_position_js_simulator.js" type="text/javascript" charset="utf-8"></script>-->
<script src="<?php echo base_url();?>js/contact.js"></script>


</head>
<body>
<header>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Bienvenido</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">Excursiones Pendientes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">Chat profes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">Horarios</a>
      </li>
      
    </ul>
    
    <span class="navbar-text">
       <a class="btn btn-danger" href="#" role="button">Contacta IT</a>
    </span>
  </div>
</nav>
</header>
<main>
<div class="container">
<div class="row">
<div class="col-11">
<div id='infoMapa'></div>
</div>
<div class="col-1">

  <span class="bell fa fa-bell"></span>

</div>
</div>
</div>
<div class="container" id="texto">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam elementum quis nisl in malesuada. Nam ac tincidunt ex. Donec condimentum tellus purus, non porta nibh imperdiet eget. Vestibulum fringilla cursus urna quis tincidunt. Morbi id mauris ac mauris scelerisque varius nec non lacus. Pellentesque luctus porta turpis non elementum. Nunc quam risus, semper in massa a, dictum semper justo. Cras mattis congue hendrerit. Suspendisse dapibus diam sit amet sapien fringilla, vitae iaculis ipsum sodales. Nunc vestibulum porta hendrerit. Suspendisse ornare ex non volutpat volutpat. Fusce eget pretium leo, in faucibus augue. Duis fermentum ligula lacus, ut pretium nisl viverra nec. Vestibulum suscipit, diam ut venenatis pulvinar, metus erat venenatis eros, id fermentum nunc tellus ac nisl. Quisque quis porttitor elit. Aliquam efficitur dui id scelerisque finibus.

Aliquam rutrum dolor id lorem consectetur, at tincidunt nibh lacinia. Sed ullamcorper cursus nulla, non pellentesque ex congue non. Suspendisse potenti. Aenean felis purus, ultricies at sapien eget, congue imperdiet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer id iaculis nulla. Pellentesque posuere nisl vel efficitur hendrerit. In eu venenatis purus. Donec posuere neque lacinia augue dignissim posuere. In sit amet congue augue, sit amet fermentum magna. Quisque dictum justo ut iaculis ultricies. Nunc eu erat mauris. Phasellus volutpat, metus quis dignissim sollicitudin, libero dolor sollicitudin lacus, non consequat ex elit rutrum lectus. Aliquam nisl odio, rutrum eu arcu nec, mattis mollis felis. Fusce fringilla nisi eu risus dignissim, sit amet pharetra odio placerat. Nulla tincidunt laoreet magna, ac pretium quam.

Aliquam nec odio eu diam porttitor aliquam. Suspendisse potenti. Suspendisse blandit rutrum arcu at hendrerit. In sit amet purus elit. Suspendisse id fringilla massa, et suscipit odio. Curabitur lacinia, massa eu volutpat auctor, sem sapien consectetur leo, quis tempor ante ligula sed enim. Phasellus a imperdiet mi. Sed tempor placerat eros, ut vehicula velit fermentum eget. Aliquam fermentum faucibus sapien, vitae tincidunt erat volutpat non. Aenean ultricies viverra diam, quis ultricies risus ullamcorper eget. Quisque rutrum metus imperdiet magna egestas, non elementum eros tempor. Morbi tortor dui, convallis nec sagittis eu, viverra eget ipsum. Aliquam a rhoncus nulla.

Ut ac justo sed orci condimentum tempus. Ut maximus porta augue eget laoreet. Praesent mi orci, interdum eu lorem ac, faucibus varius purus. In hac habitasse platea dictumst. Sed ullamcorper, libero ac placerat ultricies, augue sem feugiat eros, vitae tincidunt justo arcu vitae dolor. Vivamus id volutpat velit. Etiam eget mattis turpis. Sed at est nunc. Mauris nec dui id nulla posuere fermentum sit amet sed nisi. Praesent pulvinar ullamcorper metus, ac congue ipsum laoreet sit amet. Nulla nec augue eu enim interdum porttitor.

Suspendisse erat lorem, lobortis quis nulla a, feugiat luctus eros. Fusce in pretium mi. Proin ac varius eros. Donec rutrum sapien at quam scelerisque, et gravida nisl hendrerit. Fusce sit amet ipsum in dui finibus feugiat. In hac habitasse platea dictumst. Phasellus metus nibh, egestas at placerat in, feugiat a felis. Mauris pretium blandit urna ac tristique.
</div>

</main>
</body>
</html>