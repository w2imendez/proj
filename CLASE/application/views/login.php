
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="Author" content="Ivan">
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/login.css">
    <script src="<?php echo base_url();?>js/login.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>font-awesome/css/fontawesome-all.css">

		<title>
			MENUDA PRACTICA...
		</title>
		<style>


		</style>
	</head>

	<body id="body">
		<div class="container" id="clogin">
			<div class="row">
				<div class=" col-12 col-md-4    col-md-offset-1 empresa login">
					<form id="fem" name="fem " method="post" action="<?php echo site_url('Login/tutor'); ?>">
					  <div class="form-group row">
						<label for="logintutor" class="col-4 col-form-label">Usuario</label>
						<div class="col-8">
						  <input type="text" class="form-control" name="logintutor" id="logintutor" placeholder="introduce tu usuario ">
						</div>
					  </div>
					  <div class="form-group row">
						<label for="passwordtutor" class="col-4 col-form-label">Password</label>
						<div class="col-6">
						  <input type="password" class="form-control" id="passwordtutor" name="passwordtutor" placeholder="Password">
						</div>
					  </div>
						 <div class="form-group row">
							<button type="submit" class="btn btn-danger col-6" id="BtnUsuario">Login</button>
						</div>
						
				
					</form>
					 <img class="log" id="iem" src="<?php echo base_url();?>imagenes/tutor.png" alt="Login para los tutores">
				</div>
				<div class=" col-12 col-md-4  col-md-offset-2  alumne login">
					<form id="fal" name="fal" method="post" action="<?php echo site_url('Login/familia'); ?>">
					  <div class="form-group row">
						<label for="usuariofamilia" class="col-4 col-form-label">Usuari</label>
						<div class="col-8">
						  <input type="text" class="form-control" id="usuariofamilia" name="usuariofamilia" placeholder="Introduce tu usuario familiar">
						</div>
					  </div>
					  <div class="form-group row">
						<label for="inputPassword3" class="col-4 col-form-label">Password</label>
						<div class="col-8">
						  <input type="password" class="form-control" id="passwordalumno"  name="passwordalumno" placeholder="Password">
						</div>
					  </div>
						<div class="form-row align-items-right">
					
							<button type="submit" class="btn btn-danger" id="btnFamilia">Login</button>
						
					</div>
					</form>
					<img id="ial" class="log" src="<?php echo base_url();?>imagenes/alumno.png" alt="">
				</div>
			</div>
		</div>
		
		<div id="mimodal" class="container">
	<button id="adminbtn" type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal">
     <i class="fa fa-address-book" aria-hidden="true"></i>
</button>
	<!-- Modal -->
	<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" >
					<h4 class="modal-title" id="exampleModalLabel">Entra como Administrador</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<form method="post" action="<?php echo site_url('Login/admin'); ?>">
					<div class="modal-body">
						<div class="form-group">
							<div class="input-group text-center">
								<span class="input-group-addon "><i class="fa fa-user-circle" aria-hidden="true"></i></span>
								<input type="text" class="form-control" id="usernameAdmin" name="usernameAdmin" placeholder="USER">
							</div>
							<br/>
							<div class="form-group">
								<div class="input-group col-mb-6 mr-sm-2 mb-sm-0 text-center">
									<span class="input-group-addon "><i class="fa fa-key" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="passwordadmin" id="passwordadmin" placeholder="PASS" name="PASS">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Cierra</button>
							<button type="submit" class="btn btn-secondary">Login</button>
						</div>
				</form>
				</div>
			</div>
		</div>
	</div>
		</div>

	
	

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	</body>

	</html>
