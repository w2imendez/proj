
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="Author" content="Ivan">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>font-awesome/css/fontawesome-all.css">
    <script src="<?php echo base_url();?>js/admin.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/admin2.css">
		<title>
			MENUDA PRACTICA...
		</title>
    

	</head>
  <body>
  <header>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Panel de Control</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/usuario'); ?>">Usuario</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/items'); ?>">Items</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/categoria'); ?>">Categorias</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/alumno'); ?>">Alumnos</a>
      </li>
      
    </ul>
    
    <span class="navbar-text">
       <a class="btn btn-danger" href="<?php echo site_url('admin/logout'); ?>" role="button">Logout</a>
    </span>
  </div>
</nav>
</header>
<main>
<div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Importe Usuario
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
       Selecciona un Fichero .csv para importar usuarios (admins,tutores,familia) a la base  de datos!
       <form name ="f1" action="<?php echo site_url('admin/subeusuario'); ?>" method="post" enctype="multipart/form-data">
          <div class="form-row">
            <div class="col">
              <input type="file" accept=".csv" class="form-control-file" id="usuarioupload" name="usuarioupload" > 
            </div>
            <div class="col text-right">
              <button type="submit" class="btn btn-danger">Sube!</button>
            </div>  
          </div>
       </form>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Importe Items 
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        Selecciona un Fichero .csv para importar items a la base  de datos!                                                                                   
         <form name ="f2" action="<?php echo site_url('admin/subeitem'); ?>" method="post" enctype="multipart/form-data">
          <div class="form-row">
              <div class="col">
                <input type="file" accept=".csv" class="form-control-file" id="itemsupload" name="itemsupload" > 
              </div>
              <div class="col text-right">
                <button type="submit" class="btn btn-danger">Sube!</button>
              </div>  
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Importe Categorias
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        Selecciona un Fichero .csv para importar categorias a la base de datos!                                                                           
        <form name ="f3" action="<?php echo site_url('admin/subecategoria'); ?>" method="post" enctype="multipart/form-data">
          <div class="form-row">
              <div class="col">
                <input type="file" accept=".csv" class="form-control-file" id="categoriaupload" name="categoriaupload" > 
              </div>
              <div class="col text-right">
                <button type="submit" class="btn btn-danger">Sube!</button>
              </div>  
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          Importe Alumnos
        </button>
      </h5>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body">
        Selecciona un Fichero .csv para importar categorias a la base de datos!                                                                           
        <form name ="f4" action="<?php echo site_url('admin/subealumno'); ?>" method="post" enctype="multipart/form-data">
          <div class="form-row">
                <div class="col">
                  <input type="file" accept=".csv" class="form-control-file" id="alumnoupload" name="alumnoupload" > 
                </div>
                <div class="col text-right">
                  <button type="submit" class="btn btn-danger">Sube!</button>
                </div>  
          </div>  
        </form>
      </div>
    </div>
  </div>
</div>
</main>
<?php
if(isset($res)) {
  if($res[0] == -1) {
    echo '<div class="alert alert-danger alert-dismissable" id="flash-msg">
<button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
<h4><i class="icon fa fa-warning"></i>Algo ha ido mal.....Seguro que has puesto un csv de '. $res[count($res)-1] .'?</h4>
</div>';

    
  }
  else {
    $cont =  count($res)-1;
    $tipo = $res[count($res)-1];
    echo '<div class="alert alert-success alert-dismissable" id="flash-msg">
<button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
<h4><i class="icon fa fa-check"></i>Genial, se han importado '. "$cont $tipo"  .' con exito!</h4>
</div>';

    
  }
  
  
}
 ?>
<div class="container" id="mitexto">
Desde este nuestro centro de la excelencia es muy importante para nosotros... 
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam elementum quis nisl in malesuada. Nam ac tincidunt ex. Donec condimentum tellus purus, non porta nibh imperdiet eget. Vestibulum fringilla cursus urna quis tincidunt. Morbi id mauris ac mauris scelerisque varius nec non lacus. Pellentesque luctus porta turpis non elementum. Nunc quam risus, semper in massa a, dictum semper justo. Cras mattis congue hendrerit. Suspendisse dapibus diam sit amet sapien fringilla, vitae iaculis ipsum sodales. Nunc vestibulum porta hendrerit. Suspendisse ornare ex non volutpat volutpat. Fusce eget pretium leo, in faucibus augue. Duis fermentum ligula lacus, ut pretium nisl viverra nec. Vestibulum suscipit, diam ut venenatis pulvinar, metus erat venenatis eros, id fermentum nunc tellus ac nisl. Quisque quis porttitor elit. Aliquam efficitur dui id scelerisque finibus.

Aliquam rutrum dolor id lorem consectetur, at tincidunt nibh lacinia. Sed ullamcorper cursus nulla, non pellentesque ex congue non. Suspendisse potenti. Aenean felis purus, ultricies at sapien eget, congue imperdiet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer id iaculis nulla. Pellentesque posuere nisl vel efficitur hendrerit. In eu venenatis purus. Donec posuere neque lacinia augue dignissim posuere. In sit amet congue augue, sit amet fermentum magna. Quisque dictum justo ut iaculis ultricies. Nunc eu erat mauris. Phasellus volutpat, metus quis dignissim sollicitudin, libero dolor sollicitudin lacus, non consequat ex elit rutrum lectus. Aliquam nisl odio, rutrum eu arcu nec, mattis mollis felis. Fusce fringilla nisi eu risus dignissim, sit amet pharetra odio placerat. Nulla tincidunt laoreet magna, ac pretium quam.
</div>
	</body>

	</html>
