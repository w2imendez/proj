<script src="<?php echo base_url(); ?>js/footer.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/footer.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<footer id="mifooter" class="page-footer font-small blue pt-4 mt-4">

    <!--Footer Links-->
    <div class="container-fluid text-center text-md-left">
        <div class="row">

            <!--First column-->
            <div class="col-md-6">
                <h5 class="text-uppercase">COLEGIO FOO BAR</h5>
                <p>El mejor colegio del mundo mundial</p>
                <p><button id="darkea" type="button" class="btn btn-danger">Dark</button><button type="button" id="lightea" class="btn btn-primary">Light</button></p>
            </div>
            <!--/.First column-->

            <!--Second column-->
            <div class="col-md-6">
                <h5 class="text-uppercase">Links</h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Nuestras instalaciones</a>
                    </li>
                    <li>
                        <a href="<?php  
                        
                        /*
                        String url = "http://92.222.27.83:8080/jasperserver/rest_v2"
                    + "/reports/DAW2/reports/ExCridaJavaAutorsAmbTitolsAmbFiltrePerAutor.pdf?"
                    + "autor=1";
                        */
                        
                        echo "http://92.222.27.83:8080/jasperserver/rest_v2/reports/w2-imendez/ReportProj.pdf";
                        ?>">Que Evaluamos?</a>
                    </li>
                    <li>
                        <a href="#!">Contactanos</a>
                    </li>
                </ul>
            </div>
            <!--/.Second column-->
        </div>
    </div>
    <!--/.Footer Links-->

    <!--Copyright-->
    <div class="footer-copyright py-3 text-center">
         2018 Copyright:
        <a href="#"> IvanMendez.com </a>
    </div>
    <!--/.Copyright-->

</footer>
</div>
</body>
</html>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

