
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="Author" content="Ivan">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>font-awesome/css/fontawesome-all.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/admin.css">
		<title>
			MENUDA PRACTICA...
		</title>
    

	</head>
  <body>
  <header>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Panel de Control</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link" href="<?php echo site_url('admin/index'); ?>">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/usuario'); ?>">Usuario</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/items'); ?>">Items</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/categoria'); ?>">Categorias</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo site_url('admin/alumno'); ?>">Alumnos <span class="sr-only">(current)</span></a>
      </li>
    </ul>
    

    <span class="navbar-text">
       <a class="btn btn-danger" href="<?php echo site_url('admin/logout'); ?>" role="button">Logout</a>
    </span>
  </div>
</nav>
</header>
<main>
<div class="container cartas">
			<?php $i=0; ?>
			<?php if (isset($alumnos)) { ?>
			<div class="card-deck">
				<?php foreach ($alumnos as $data) { ?>
				<?php if(($i%2)==0) echo "</div><div class='card-deck'>"; ?>
				<div class="card border-faded">
					<div class="card-body">
						<div class="row">
							<div class="col-4 text-left">
								<h5 class="card-title  text-left ">
									<?php echo $data['nom']; ?>
								</h5>
							</div>
							
							<div class="col-8 text-right">
								<h5 class="card-title  text-right">
									<?php echo $data['tutor']; ?>
								</h5>

								</div>
						</div>
						<div class="row">
              <div class="col-4 text-left">
								<p class="card-text">
									COGNOM
								</p>
							</div>
							<div class="col-8 text-right">
								<p class="card-text ">
									
                  <?php echo $data['cognom'] ?>
								</p>
							</div>
						</div>
            <div class="row">
              <div class="col-4 text-left">
								<p class="card-text">
									COGNOM2
								</p>
							</div>
							<div class="col-8 text-right">
								<p class="card-text ">
									
                  <?php echo $data['cognom2'] ?>
								</p>
							</div>
						</div>
            </div>
            <div class="card-footer clearfix">
                Familia:   <?php echo $data['familia'] ?>
					
				     </div>
				</div>
				<?php $i++; ?>
				<?php } ?>
				<?php } else { ?>
				<div><div class="alert alert-danger text-center" style="margin:15px;" role="alert">No existen Usuarios!</div></div>
			<?php } ?>
			</div>
		</div>



        

</main>
	</body>

	</html>
