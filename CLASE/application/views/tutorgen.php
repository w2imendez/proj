
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="Author" content="Ivan">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>font-awesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/tutor.css">
    <script src="<?php echo base_url(); ?>/js/tutor.js"></script>
		<title>
			MENUDA PRACTICA...
		</title>
    

	</head>
  <body>
  <header>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Panel de Control de <?php echo $tut ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo site_url('Tutor/index'); ?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">Mis Alumnos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">Mi clases</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">Mis horarios</a>
      </li>
      
    </ul>
    
    <span class="navbar-text">
       <a class="btn btn-danger" href="<?php echo site_url('Tutor/logout'); ?>" role="button">Logout</a>
    </span>
  </div>
</nav>
</header>
<main>
<form class="form" method="POST" id="contform"action="<?php echo site_url('Tutor/filtro'); ?>">
   <div id="contsugerencias">
<div class="input-group mb-3">
  <div id="pruebasbox" class="input-group-prepend">
    <button class="btn btn-outline-secondary" type="submit">Nombre</button>
  </div>
  <!--- position relative -->
    <input type="text" id="autocompleta" name="autocompleta" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1">
    <div id="suggesstion-box">SUGERENCIAS</div> <!--- position absolute absolute top 100%-->

</div>
  </div>
</form>
<input type="hidden" id="secreto" value="<?php echo base_url(); ?>">
<div class="container cartas">
			<?php $i=0; ?>
			<?php if (isset($res)) { ?>
			<div class="card-deck">
				<?php foreach ($res as $data) { ?>
				<?php if(($i%2)==0) echo "</div><div class='card-deck'>"; ?>
				<div class="card border-faded">
					<div class="card-body">
						<div class="row">
							<div class="col-4 text-left">
								<h5 class="card-title  text-left ">
									NOM:
								</h5>
							</div>
							
							<div class="col-8 text-right">
								<h5 class="card-title  text-right">
									<?php echo $data['nom']; ?>
								</h5>

								</div>
						</div>
						<div class="row">
						<div class="col-4 text-left">
								<p class="card-text">
									COGNOM
								</p>
							</div>
							<div class="col-8 text-right">
								<p class="card-text ">
									
                  <?php echo $data['cognom'] ?>
								</p>
							</div>
							
						</div>
            <div class="row">
              <div class="col-5 text-left">
								<p class="card-text">
									 COGNOM2
								</p>
							</div>
							<div class="col-7 text-right">
								<p class="card-text ">
								
                  <?php echo $data['cognom2'] ?>
								</p>
							</div>
							
						</div>
                        <br>
            <hr>
            <div class="row">
              <div class="col-2 text-left">
								<p class="card-text">
									 T1
								</p>
							</div>
							<div class="col-10 text-right">
								<p class="card-text ">
								
                  <?php if($data['t1'] == 0) {?>
                    <a href="<?php echo site_url('Tutor/creaevaluacion/'. $data["id"] . "/1"); ?>" class="btn btn-primary btn-sm " role="button" aria-disabled="false">Crea</a>
                  <?php  } else { ?>
                     <a href="<?php echo site_url('Tutor/modificaevaluacion/'. $data["id"] . "/1"); ?>" class="btn btn-primary btn-sm " role="button" aria-disabled="false">Edita</a>
                      <a href="<?php echo site_url('Tutor/eliminaevaluacion/'. $data["id"] . "/1"); ?>" class="btn btn-danger btn-sm " role="button" aria-disabled="false">Borrame</a>
                      <a href="<?php echo site_url('Tutor/descargaevaluacion/'. $data["id"] . "/1"); ?>" class="btn btn-warning btn-sm " role="button" aria-disabled="false">Visualizame</a>
                     <?php }?>
								</p>
							</div>
							
						</div>

            <hr>
            <div class="row">
              <div class="col-2 text-left">
								<p class="card-text">
									 T2
								</p>
							</div>
							<div class="col-10 text-right">
								<p class="card-text ">
								
                  <?php if($data['t2'] == 0) {?>
                    <a href="<?php echo site_url('Tutor/creaevaluacion/'. $data["id"] . "/2"); ?>" class="btn btn-primary btn-sm " role="button" aria-disabled="false">Crea</a>
                  <?php  } else { ?>
                     <a href="<?php echo site_url('Tutor/modificaevaluacion/'. $data["id"] . "/2"); ?>" class="btn btn-primary btn-sm " role="button" aria-disabled="false">Edita</a>
                      <a href="<?php echo site_url('Tutor/eliminaevaluacion/'. $data["id"] . "/2"); ?>" class="btn btn-danger btn-sm " role="button" aria-disabled="false">Borrame</a>
                      <a href="<?php echo site_url('Tutor/descargaevaluacion/'. $data["id"] . "/2"); ?>" class="btn btn-warning btn-sm " role="button" aria-disabled="false">Visualizame</a>
                     <?php }?>
								</p>
							</div>
							
						</div>
            <hr>
            <div class="row">
              <div class="col-2 text-left">
								<p class="card-text">
									 T3
								</p>
							</div>
							<div class="col-10 text-right">
								<p class="card-text ">
								
                  <?php if($data['t3'] == 0) {?>
                    <a href="<?php echo site_url('Tutor/creaevaluacion/'. $data["id"] . "/3"); ?>" class="btn btn-primary btn-sm " role="button" aria-disabled="false">Crea</a>
                  <?php  } else { ?>
                     <a href="<?php echo site_url('Tutor/modificaevaluacion/'. $data["id"] . "/3"); ?>" class="btn btn-primary btn-sm " role="button" aria-disabled="false">Edita</a>
                      <a href="<?php echo site_url('Tutor/eliminaevaluacion/'. $data["id"] . "/3"); ?>" class="btn btn-danger btn-sm " role="button" aria-disabled="false">Borrame</a>
                      <a href="<?php echo site_url('Tutor/descargaevaluacion/'. $data["id"] . "/3"); ?>" class="btn btn-warning btn-sm " role="button" aria-disabled="false">Visualizame</a>
                     <?php }?>
								</p>
							</div>
							
						</div>
					
						
					</div>
					<div class="card-footer clearfix">
           
				     </div>
				</div>
				<?php $i++; ?>
				<?php } ?>
				<?php } else { ?>
				<div><div class="alert alert-danger text-center" style="margin:15px;" role="alert">No existen Usuarios!</div></div>
			<?php } ?>
			</div>
		</div>



</main>
<?php
if(!isset($res)) {
  echo '<div class="alert alert-danger alert-dismissable" id="flash-msg">
<button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
<h4><i class="icon fa fa-warning"></i>No Hay Alumnos, que has tocado?</h4>
</div>';
}
 ?>

	</body>

	</html>
