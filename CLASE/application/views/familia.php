
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="Author" content="Ivan">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>font-awesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/familia.css">
    <script src="<?php echo base_url(); ?>/js/familia.js"></script>
		<title>
			MENUDA PRACTICA...
		</title>
    

	</head>
  <body>
  <header>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Bienvenido <?php echo $familia ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">Excursiones Pendientes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">Chat profes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">Horarios</a>
      </li>
      
    </ul>
    
    <span class="navbar-text">
       <a class="btn btn-danger" href="<?php echo site_url('Familia/logout'); ?>" role="button">Logout</a>
    </span>
  </div>
</nav>
</header>
<main>
<input type="hidden" id="secreto" value="<?php echo base_url(); ?>">
<div class="container cartas">
			<?php $i=0; ?>
			<?php if (isset($res)) { ?>
			<div class="card-deck">
				<?php foreach ($res as $data) { ?>
				<?php if(($i%2)==0) echo "</div><div class='card-deck'>"; ?>
				<div class="card border-faded">
					<div class="card-body">
						<div class="row">
							<div class="col-4 text-left">
								<h5 class="card-title  text-left ">
									NOM:
								</h5>
							</div>
							
							<div class="col-8 text-right">
								<h5 class="card-title  text-right">
									<?php echo $data['nom']; ?>
								</h5>

								</div>
						</div>
						<div class="row">
						<div class="col-4 text-left">
								<p class="card-text">
									COGNOM
								</p>
							</div>
							<div class="col-8 text-right">
								<p class="card-text ">
									
                  <?php echo $data['cognom'] ?>
								</p>
							</div>
							
						</div>
            <div class="row">
              <div class="col-5 text-left">
								<p class="card-text">
									 COGNOM2
								</p>
							</div>
							<div class="col-7 text-right">
								<p class="card-text ">
								
                  <?php echo $data['cognom2'] ?>
								</p>
							</div>
							
						</div>
                        <br>
            <hr>
            <div class="row">
              <div class="col-2 text-left">
								<p class="card-text">
									 T1
								</p>
							</div>
							<div class="col-10 text-right">
								<p class="card-text ">
								
                  <?php if($data['t1'] != 0) {?>
                    <a href="<?php echo site_url('Tutor/descargaevaluacion/'. $data["id"] . "/1"); ?>" class="btn btn-primary btn-sm " role="button" aria-disabled="false">Visualiza</a>
                  <?php  } else { ?>
                     
                     <?php }?>
								</p>
							</div>
							
						</div>

            <hr>
            <div class="row">
              <div class="col-2 text-left">
								<p class="card-text">
									 T2
								</p>
							</div>
							<div class="col-10 text-right">
								<p class="card-text ">
								
                  <?php if($data['t2'] != 0) {?>
                    <a href="<?php echo site_url('Tutor/descargaevaluacion/'. $data["id"] . "/2"); ?>" class="btn btn-primary btn-sm " role="button" aria-disabled="false">Visualiza</a>
                  <?php  } else { ?>
                    
                     <?php }?>
								</p>
							</div>
							
						</div>
            <hr>
            <div class="row">
              <div class="col-2 text-left">
								<p class="card-text">
									 T3
								</p>
							</div>
							<div class="col-10 text-right">
								<p class="card-text ">
								
                  <?php if($data['t3'] != 0) {?>
                    <a href="<?php echo site_url('Tutor/descargaevaluacion/'. $data["id"] . "/3"); ?>" class="btn btn-primary btn-sm " role="button" aria-disabled="false">Visualiza</a>
                  <?php  } else { ?>
                     
                     <?php }?>
								</p>
							</div>
							
						</div>
					
						
					</div>
					<div class="card-footer clearfix">
           
				     </div>
				</div>
				<?php $i++; ?>
				<?php } ?>
				<?php } else { ?>
				<div><div class="alert alert-danger text-center" style="margin:15px;" role="alert">No existen Hijos!</div></div>
			<?php } ?>
			</div>
		</div>
<div class="container" id="mitexto">
Desde este nuestro centro de la excelencia es muy importante para nosotros... 
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam elementum quis nisl in malesuada. Nam ac tincidunt ex. Donec condimentum tellus purus, non porta nibh imperdiet eget. Vestibulum fringilla cursus urna quis tincidunt. Morbi id mauris ac mauris scelerisque varius nec non lacus. Pellentesque luctus porta turpis non elementum. Nunc quam risus, semper in massa a, dictum semper justo. Cras mattis congue hendrerit. Suspendisse dapibus diam sit amet sapien fringilla, vitae iaculis ipsum sodales. Nunc vestibulum porta hendrerit. Suspendisse ornare ex non volutpat volutpat. Fusce eget pretium leo, in faucibus augue. Duis fermentum ligula lacus, ut pretium nisl viverra nec. Vestibulum suscipit, diam ut venenatis pulvinar, metus erat venenatis eros, id fermentum nunc tellus ac nisl. Quisque quis porttitor elit. Aliquam efficitur dui id scelerisque finibus.

Aliquam rutrum dolor id lorem consectetur, at tincidunt nibh lacinia. Sed ullamcorper cursus nulla, non pellentesque ex congue non. Suspendisse potenti. Aenean felis purus, ultricies at sapien eget, congue imperdiet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer id iaculis nulla. Pellentesque posuere nisl vel efficitur hendrerit. In eu venenatis purus. Donec posuere neque lacinia augue dignissim posuere. In sit amet congue augue, sit amet fermentum magna. Quisque dictum justo ut iaculis ultricies. Nunc eu erat mauris. Phasellus volutpat, metus quis dignissim sollicitudin, libero dolor sollicitudin lacus, non consequat ex elit rutrum lectus. Aliquam nisl odio, rutrum eu arcu nec, mattis mollis felis. Fusce fringilla nisi eu risus dignissim, sit amet pharetra odio placerat. Nulla tincidunt laoreet magna, ac pretium quam.

Aliquam nec odio eu diam porttitor aliquam. Suspendisse potenti. Suspendisse blandit rutrum arcu at hendrerit. In sit amet purus elit. Suspendisse id fringilla massa, et suscipit odio. Curabitur lacinia, massa eu volutpat auctor, sem sapien consectetur leo, quis tempor ante ligula sed enim. Phasellus a imperdiet mi. Sed tempor placerat eros, ut vehicula velit fermentum eget. Aliquam fermentum faucibus sapien, vitae tincidunt erat volutpat non. Aenean ultricies viverra diam, quis ultricies risus ullamcorper eget. Quisque rutrum metus imperdiet magna egestas, non elementum eros tempor. Morbi tortor dui, convallis nec sagittis eu, viverra eget ipsum. Aliquam a rhoncus nulla.

Ut ac justo sed orci condimentum tempus. Ut maximus porta augue eget laoreet. Praesent mi orci, interdum eu lorem ac, faucibus varius purus. In hac habitasse platea dictumst. Sed ullamcorper, libero ac placerat ultricies, augue sem feugiat eros, vitae tincidunt justo arcu vitae dolor. Vivamus id volutpat velit. Etiam eget mattis turpis. Sed at est nunc. Mauris nec dui id nulla posuere fermentum sit amet sed nisi. Praesent pulvinar ullamcorper metus, ac congue ipsum laoreet sit amet. Nulla nec augue eu enim interdum porttitor.

Suspendisse erat lorem, lobortis quis nulla a, feugiat luctus eros. Fusce in pretium mi. Proin ac varius eros. Donec rutrum sapien at quam scelerisque, et gravida nisl hendrerit. Fusce sit amet ipsum in dui finibus feugiat. In hac habitasse platea dictumst. Phasellus metus nibh, egestas at placerat in, feugiat a felis. Mauris pretium blandit urna ac tristique.
</div>


</main>
<?php
if(!isset($res)) {
  echo '<div class="alert alert-danger alert-dismissable" id="flash-msg">
<button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
<h4><i class="icon fa fa-warning"></i>No Hay hijos, que habra pasado?</h4>
</div>';
}
 ?>

	</body>

	</html>
