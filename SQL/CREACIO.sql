CREATE TABLE USUARIO (
id int PRIMARY KEY,
nom varchar(100) NOT NULL,
cognom varchar(100) NOT NULL,
cognom2 varchar(100) NOT NULL,
login varchar(100) NOT NULL,
password varchar(100) NOT NULL,
mail varchar(100) NOT NULL
);

CREATE TABLE ROL (
codi int PRIMARY KEY,
nom varchar(100) NOT NULL,
descripcio varchar(200) NOT NULL
);

CREATE TABLE UR (
usuario int,
rol int,
FOREIGN KEY (usuario) REFERENCES USUARIO(id),
FOREIGN KEY (rol) REFERENCES ROL(codi),
primary key (usuario, rol)
);

CREATE TABLE ALUMNE (
id int PRIMARY KEY,
nom varchar(100) NOT NULL,
cognom varchar(100) NOT NULL,
cognom2 varchar(100) NOT NULL,
familia int,
tutor int,
FOREIGN KEY (familia) REFERENCES USUARIO(id),
FOREIGN KEY (tutor) REFERENCES USUARIO(id)
);

CREATE TABLE CATEGORIA (
id int primary key,
nom varchar(100) NOT NULL
);

CREATE TABLE ITEM (
id int primary key,
nom varchar(100) NOT NULL,
categoria int,
FOREIGN KEY (categoria) REFERENCES CATEGORIA(id)

);

CREATE TABLE EVALUACION (
usuario int,
alumne int,
item int,
trimestre int,
valoracion int,
FOREIGN KEY (usuario) REFERENCES USUARIO(id),
FOREIGN KEY (alumne) REFERENCES ALUMNE(id),
FOREIGN KEY (item) REFERENCES ITEM(id),
primary key (usuario,alumne,item,trimestre)
);


